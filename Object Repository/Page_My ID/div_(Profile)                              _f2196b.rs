<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Profile)                              _f2196b</name>
   <tag></tag>
   <elementGuidId>5e734acc-4d96-4c65-a85e-2c2a77fb5127</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.row</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>row</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)141990195****
            รหัสนิสิต (Student code)
          62160272
        
        
          ชื่อ - นามสกุล (Name)
          Thanyarak Namwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-08 19:11:02 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:55
        
    

 

 


			 
	

                    
                    
                
                
                
            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)141990195****
            รหัสนิสิต (Student code)
          62160272
        
        
          ชื่อ - นามสกุล (Name)
          Thanyarak Namwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-08 19:11:02 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:55
        
    

 

 


			 
	

                    
                    
                
                
                
            ' or . = '
                
                	ข้อมูลส่วนบุคคล (Profile)


    
        
                    หมายเลขบัตรประชาชน (Citizen ID)141990195****
            รหัสนิสิต (Student code)
          62160272
        
        
          ชื่อ - นามสกุล (Name)
          Thanyarak Namwong
          คณะ (Faculty)Informatics
        
        
          รหัสผ่านหมดอายุ (Password Expire)2022-09-08 19:11:02 (balance : 179 days)          
          บัญชีผู้ใช้หมดอายุ (Account Expire)2572-06-22 02:02:55
        
    

 

 


			 
	

                    
                    
                
                
                
            ')]</value>
   </webElementXpaths>
</WebElementEntity>
